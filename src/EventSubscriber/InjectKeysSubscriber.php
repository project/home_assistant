<?php

namespace Drupal\home_assistant\EventSubscriber;

use Drupal\http_client_manager\Event\HttpClientEvents;
use Drupal\http_client_manager\Event\HttpClientHandlerStackEvent;
use Drupal\key\KeyRepositoryInterface;
use GuzzleHttp\Middleware;
use GuzzleHttp\Psr7\Uri;
use Psr\Http\Message\RequestInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Inject keys to Home Assistant API-requests.
 */
class InjectKeysSubscriber implements EventSubscriberInterface {

  /**
   * A collection of keys.
   *
   * @var \Drupal\key\KeyInterface[]
   */
  protected array $keys = [];

  /**
   * Creates an AddAuthorizationSubscriber-instance.
   *
   * @param \Drupal\key\KeyRepositoryInterface $keyRepository
   *   The key repository.
   */
  public function __construct(
    protected KeyRepositoryInterface $keyRepository
  ) {
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array {
    return [
      HttpClientEvents::HANDLER_STACK => 'onHandlerStack',
    ];
  }

  /**
   * Acts on the http_client.handler_stack-event.
   *
   * @param \Drupal\http_client_manager\Event\HttpClientHandlerStackEvent $event
   *   The event.
   */
  public function onHandlerStack(HttpClientHandlerStackEvent $event): void {
    if ($event->getHttpServiceApi() !== 'home_assistant_services') {
      return;
    }

    $handler = $event->getHandlerStack();

    // Base URI.
    $key = $this->keyRepository->getKey('ha_base_uri');
    if (!empty($key) && !empty($key->getKeyValue())) {
      $this->keys[$key->id()] = $key;
      $middleware = Middleware::mapRequest([$this, 'setBaseUri']);
      $handler->push($middleware, 'home_assistant_services');
    }

    // Authorization.
    $key = $this->keyRepository->getKey('ha_api_key');
    if (!empty($key) && !empty($key->getKeyValue())) {
      $this->keys[$key->id()] = $key;
      $middleware = Middleware::mapRequest([$this, 'addAuthorization']);
      $handler->push($middleware, 'home_assistant_services');
    }
  }

  /**
   * Set the base URI of the http-request.
   *
   * @param \Psr\Http\Message\RequestInterface $request
   *   The http-request.
   *
   * @return \Psr\Http\Message\RequestInterface
   *   Returns the altered http-request.
   */
  public function setBaseUri(RequestInterface $request): RequestInterface {
    return $request->withUri(new Uri($this->keys['ha_base_uri']->getKeyValue()));
  }

  /**
   * Add authorization to the http-request.
   *
   * @param \Psr\Http\Message\RequestInterface $request
   *   The http-request.
   *
   * @return \Psr\Http\Message\RequestInterface
   *   Returns the altered http-request.
   */
  public function addAuthorization(RequestInterface $request): RequestInterface {
    return $request->withHeader('authorization', sprintf('Bearer %s', $this->keys['ha_api_key']->getKeyValue()));
  }

}
