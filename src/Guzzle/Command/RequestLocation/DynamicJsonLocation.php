<?php

namespace Drupal\home_assistant\Guzzle\Command\RequestLocation;

use GuzzleHttp\Command\CommandInterface;
use GuzzleHttp\Command\Guzzle\Parameter;
use GuzzleHttp\Command\Guzzle\RequestLocation\BodyLocation;
use GuzzleHttp\Command\Guzzle\RequestLocation\JsonLocation;
use GuzzleHttp\Psr7\Utils;
use Psr\Http\Message\RequestInterface;
use Symfony\Component\VarDumper\VarDumper;

class DynamicJsonLocation extends JsonLocation {

  /**
   * {@inheritdoc}
   */
  public function __construct($locationName = 'dynamic_json', $contentType = 'application/json') {
    parent::__construct($locationName, $contentType);
  }

  /**
   * {@inheritdoc}
   */
  public function visit(CommandInterface $command, RequestInterface $request, Parameter $param) {
    $value = $command[$param->getName()];
    $value = $param->filter($value);
    VarDumper::dump($value);

    return $request->withBody(Utils::streamFor($value));
  }

}
